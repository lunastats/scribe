package scribe

import (
	"fmt"
	"net/http"
)

func redirectHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, Scribe!")
}

func ListenAndServe(addr string) error {
	http.HandleFunc("/r", redirectHandler)
	return http.ListenAndServe(addr, nil)
}
