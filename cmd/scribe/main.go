package main

import (
	"log"

	"lunastats.com/scribe"
)

func main() {
	log.Fatal(scribe.ListenAndServe(":3000"))
}
